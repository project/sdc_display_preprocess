<?php

/**
 * @file
 * Preprocess override for a field group rendering itself with this component.
 *
 * Here you can modify props and slots before they are sent to the component
 * template.
 *
 * The loading of this file is provided by the SDC Display Preprocess module.
 * @see sdc_display_preprocess.module
 *
 * Available variables:
 * @var array $props
 *   The props array that will be sent to the component template.
 *   Any modifications here will be sent to templates.
 * @var array $slots
 *   The slots array that will be sent to the component template.
 *   Any modifications here will be sent to templates.
 * @var array $element
 *   Group being rendered.
 * @var object $group
 *   The Field group info.
 * @var object $rendering_object
 *   The entity / form being rendered.
 */

// Preprocess your variables here.
$props['foo'] = 'newProp';
$slots['bar'] = 'newSlot';
