<?php

/**
 * @file
 * Preprocess override for an entity rendering itself with this component.
 *
 * Here you can modify props and slots before they are sent to the component
 * template.
 *
 * The loading of this file is provided by the SDC Display Preprocess module.
 * @see sdc_display_preprocess.module
 *
 * Available variables:
 * @var array $props
 *   The props array that will be sent to the component template.
 *   Any modifications here will be sent to templates.
 * @var array $slots
 *   The slots array that will be sent to the component template.
 *   Any modifications here will be sent to templates.
 * @var array $build
 *   The entity's render array.
 *   Any modifications here will be sent to templates.
 * @var \Drupal\Core\Entity\EntityInterface $entity
 *   The original entity being rendered.
 * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
 *   The entity view display.
 */

// Preprocess your variables here.
$props['foo'] = 'newProp';
$slots['bar'] = 'newSlot';
