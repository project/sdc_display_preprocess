<?php

/**
 * @file
 * Preprocess override for a field rendering itself with this component.
 *
 * Here you can modify props and slots before they are sent to the component
 * template.
 *
 * The loading of this file is provided by the SDC Display Preprocess module.
 * @see sdc_display_pp.module
 *
 * Available variables:
 * @var array $items
 *   The field items. Any modifications here will be sent to templates.
 * @var array $variables
 *   The original preprocess variables for the field.
 */

// Preprocess your variables here.
foreach ($items as $item) {
  $item['content']['#props']['foo'] = 'newProp';
  $item['content']['#slots']['bar'] = 'newSlot';
}
